package com.example.minh.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.minh.news.ui.NewsActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout relativeDanTri;
    private RelativeLayout relativeVNExpress;
    private RelativeLayout relativeThanhNien;
    private RelativeLayout relativeTuoiTre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeDanTri = (RelativeLayout) findViewById(R.id.dantri);
        relativeVNExpress = (RelativeLayout) findViewById(R.id.vnexpress);
        relativeThanhNien = (RelativeLayout) findViewById(R.id.thanhnien);
        relativeTuoiTre = (RelativeLayout) findViewById(R.id.vietnamnet);
        relativeDanTri.setOnClickListener(this);
        relativeVNExpress.setOnClickListener(this);
        relativeThanhNien.setOnClickListener(this);
        relativeTuoiTre.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, NewsActivity.class);

        switch(v.getId()) {
            case R.id.dantri:
                intent.putExtra("news", "dantri");
                break;

            case R.id.vnexpress:
                intent.putExtra("news", "vnexpress");
                break;

            case R.id.thanhnien:
                intent.putExtra("news", "thanhnien");
                break;

            case R.id.vietnamnet:
                intent.putExtra("news", "vietnamnet");
                break;
        }

        startActivity(intent);
    }
}
