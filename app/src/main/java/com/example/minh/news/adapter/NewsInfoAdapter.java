package com.example.minh.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minh.news.R;
import com.example.minh.news.object.NewsItem;
import com.example.minh.news.ui.WebViewActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Minh on 7/5/2016.
 */
public class NewsInfoAdapter extends RecyclerView.Adapter<NewsInfoAdapter.MyViewHolder> {
    private ArrayList<NewsItem> items;
    private Context context;
    private String news;

    public NewsInfoAdapter(Context context, ArrayList<NewsItem> items, String news) {
        this.context = context;
        this.items = items;
        this.news = news;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.news_info_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final NewsItem item = items.get(position);
        holder.title.setText(item.getmTitle());

        Date date = new Date(item.getmPubDate());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(new Date(date.getTime()));
        holder.date.setText(dateString);

        if(news.compareTo("vnexpress") == 0) {
            holder.image.setImageResource(R.drawable.ic_express);
        } else if(news.compareTo("vietnamnet") == 0) {
            holder.image.setImageResource(R.drawable.ic_vietnamnet_logo);
        } else if(news.compareTo("thanhnien") == 0) {
            holder.image.setImageResource(R.drawable.ic_thanhnien_logo);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("link", item.getmLink());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, date;
        private ImageView image;
        private CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            image = (ImageView) view.findViewById(R.id.image);
            cardView = (CardView) view.findViewById(R.id.cardView);
        }
    }
}
