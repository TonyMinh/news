package com.example.minh.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minh.news.R;
import com.example.minh.news.object.NewsType;

import java.util.ArrayList;

/**
 * Created by Minh on 7/5/2016.
 */
public class NewsTypeAdapter extends BaseAdapter {
    private ArrayList<NewsType> newsTypes;
    private Context context;
    private String news;

    public NewsTypeAdapter(Context context, ArrayList<NewsType> newsTypes, String news){
        this.context = context;
        this.newsTypes = newsTypes;
        this.news = news;
    }
    @Override
    public int getCount() {
        return newsTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return newsTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.news_type_row, parent, false);

            viewHolder.type = (TextView) convertView.findViewById(R.id.type);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.type.setText(newsTypes.get(position).getmType());

        if(news.compareTo("vnexpress") == 0) {
            viewHolder.image.setImageResource(R.drawable.ic_express);
        } else if(news.compareTo("vietnamnet") == 0) {
            viewHolder.image.setImageResource(R.drawable.ic_vietnamnet_logo);
        } else if(news.compareTo("thanhnien") == 0) {
            viewHolder.image.setImageResource(R.drawable.ic_thanhnien_logo);
        }

        return convertView;
    }

    class ViewHolder{
        private TextView type;
        private ImageView image;
    }
}