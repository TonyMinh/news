package com.example.minh.news.object;

/**
 * Created by Minh on 7/4/2016.
 */
public class NewsItem {
    private String mTitle;
    private String mLink;
    private String mDescription;
    private String mPubDate;
    private String mThumbnailURL;

    public String getmTitle() {
        return mTitle;
    }

    public String getmLink() {
        return mLink;
    }

    public String getmDescription() {
        return mDescription;
    }

    public String getmPubDate() {
        return mPubDate;
    }

    public String getmThumbnailURL() {
        return mThumbnailURL;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setmLink(String mLink) {
        this.mLink = mLink;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setmPubDate(String mPubDate) {
        this.mPubDate = mPubDate;
    }

    public void setmThumbnailURL(String mThumbnailURL) {
        this.mThumbnailURL = mThumbnailURL;
    }
}
