package com.example.minh.news.object;

/**
 * Created by Minh on 7/5/2016.
 */
public class NewsType {
    private String mType;
    private String mLink;

    public String getmType() {
        return mType;
    }

    public String getmLink() {
        return mLink;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public void setmLink(String mLink) {
        this.mLink = mLink;
    }
}
