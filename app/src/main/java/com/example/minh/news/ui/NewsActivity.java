package com.example.minh.news.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.minh.news.R;
import com.example.minh.news.adapter.NewsTypeAdapter;
import com.example.minh.news.object.NewsType;
import com.example.minh.news.utils.Values;

import java.util.ArrayList;

public class NewsActivity extends AppCompatActivity {
    private ListView list;
    private ArrayList<NewsType> newsTypes;
    private NewsTypeAdapter adapter;
    private String news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        news = getIntent().getStringExtra("news");
        if(news.compareTo("vnexpress") == 0) {
            newsTypes = Values.setVNExpressType();
        } else if(news.compareTo("vietnamnet") == 0) {
            newsTypes = Values.setVietNamNetType();
        } else if(news.compareTo("thanhnien") == 0) {
            newsTypes = Values.setThanhNienType();
        }

        list = (ListView) findViewById(R.id.list);
        adapter = new NewsTypeAdapter(this, newsTypes, news);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NewsActivity.this, NewsInfoActivity.class);
                intent.putExtra("link", newsTypes.get(position).getmLink());
                intent.putExtra("news", news);
                startActivity(intent);
            }
        });
    }
}
