package com.example.minh.news.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.example.minh.news.R;
import com.example.minh.news.utils.RssReader;

public class NewsInfoActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RssReader rssReader;
    private String address;
    private String news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_info);

        news = getIntent().getStringExtra("news");
        address = getIntent().getStringExtra("link");
        recyclerView = (RecyclerView) findViewById(R.id.recycle);
        rssReader = new RssReader(this, recyclerView, address, news);
        rssReader.execute();
    }
}
