package com.example.minh.news.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.example.minh.news.R;

public class WebViewActivity extends AppCompatActivity {
    private WebView webView;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        webView = (WebView) findViewById(R.id.webView);
        link = getIntent().getStringExtra("link");
        webView.loadUrl(link);
    }
}
