package com.example.minh.news.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.minh.news.adapter.NewsInfoAdapter;
import com.example.minh.news.object.NewsItem;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Minh on 7/5/2016.
 */
public class RssReader extends AsyncTask<Void, Void, Void> {
    private Context context;
    private ProgressDialog progressDialog;
    private String address;
    private URL url;
    private ArrayList<NewsItem> items;
    private RecyclerView recyclerView;
    private String news;

    public RssReader(Context context, RecyclerView recyclerView, String address, String news) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Đang tải...");
        this.recyclerView = recyclerView;
        this.address = address;
        this.news = news;
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        ProcessXml (getData());
        return null;
    }

    private void ProcessXml(Document document) {
        if(document != null) {
            items = new ArrayList<>();
            Element root = document.getDocumentElement();
            Node channel = null;
            if(news.compareTo("vnexpress") == 0) {
                channel = root.getChildNodes().item(1);
            } else if(news.compareTo("vietnamnet") == 0 || news.compareTo("thanhnien") == 0) {
                channel = root.getChildNodes().item(0);
            }
            NodeList item = channel.getChildNodes();
            for (int i = 0 ; i < item.getLength() ; i++) {
                Node currentChild = item.item(i);
                if(currentChild.getNodeName().equalsIgnoreCase("item")) {
                    NewsItem feedItem = new NewsItem();
                    NodeList itemChilds = currentChild.getChildNodes();
                    for (int j = 0 ; j < itemChilds.getLength() ; j++) {
                        Node current = itemChilds.item(j);
                        if(current.getNodeName().equalsIgnoreCase("title")) {
                            feedItem.setmTitle(current.getTextContent());
                        } else if(current.getNodeName().equalsIgnoreCase("link")) {
                            feedItem.setmLink(current.getTextContent());
                        } else if(current.getNodeName().equalsIgnoreCase("pubDate")) {
                            feedItem.setmPubDate(current.getTextContent());
                        } else if(current.getNodeName().equalsIgnoreCase("description")) {
                            feedItem.setmDescription(current.getTextContent());
                        } else if(current.getNodeName().equalsIgnoreCase("media:thumbnail")) {
                            String url = current.getAttributes().item(0).getTextContent();
                            feedItem.setmThumbnailURL(url);
                        }
                    }
                    items.add(feedItem);
                }
            }
        }
    }

    public Document getData() {
        try {
            url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream inputStream = connection.getInputStream();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            return documentBuilder.parse(inputStream);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        NewsInfoAdapter adapter = new NewsInfoAdapter(context, items, news);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new VerticalSpace(32));
        recyclerView.setAdapter(adapter);
    }
}