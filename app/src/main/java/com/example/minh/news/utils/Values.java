package com.example.minh.news.utils;

import com.example.minh.news.object.NewsType;

import java.util.ArrayList;

/**
 * Created by Minh on 7/5/2016.
 */
public class Values {
    // thanhnien
    public static ArrayList setThanhNienType() {
        ArrayList<NewsType> newsTypes = new ArrayList<>();
        NewsType info;
        String[] type = setTNType();
        String[] link = setTNLink();
        for(int i = 0 ; i < 14 ; i++) {
            info = new NewsType();
            info.setmType(type[i]);
            info.setmLink(link[i]);
            newsTypes.add(info);
        }
        return newsTypes;
    }

    private static String[] setTNType() {
        String[] type = new String[14];
        type[0] = "Quốc Phòng";
        type[1] = "Pháp Luật";
        type[2] = "Tình Huống";
        type[3] = "Thế Giới";
        type[4] = "Văn Hóa";
        type[5] = "Thể Thao";
        type[6] = "Đời Sống";
        type[7] = "Kinh Tế";
        type[8] = "Giới Trẻ";
        type[9] = "Giáo Dục";
        type[10] = "Công Nghệ";
        type[11] = "Game";
        type[12] = "Xe";
        type[13] = "Video";
        return type;
    }

    private static String[] setTNLink() {
        String[] type = new String[14];
        type[0] = "http://thanhnien.vn/rss/quoc-phong.rss";
        type[1] = "http://thanhnien.vn/rss/phap-luat.rss";
        type[2] = "http://thanhnien.vn/rss/T%C3%ACnh%20hu%E1%BB%91ng.rss";
        type[3] = "http://thanhnien.vn/rss/the-gioi.rss";
        type[4] = "http://thanhnien.vn/rss/van-hoa-nghe-thuat.rss";
        type[5] = "http://thanhnien.vn/rss/the-thao.rss";
        type[6] = "http://thanhnien.vn/rss/doi-song.rss";
        type[7] = "http://thanhnien.vn/rss/kinh-te.rss";
        type[8] = "http://thanhnien.vn/rss/the-gioi-tre.rss";
        type[9] = "http://thanhnien.vn/rss/giao-duc.rss";
        type[10] = "http://thanhnien.vn/rss/cong-nghe-thong-tin.rss";
        type[11] = "http://thanhnien.vn/rss/game.rss";
        type[12] = "http://thanhnien.vn/rss/the-gioi-xe.rss";
        type[13] = "http://thanhnien.vn/rss/site-media.rss";
        return type;
    }

    // vietnamnet
    public static ArrayList setVietNamNetType() {
        ArrayList<NewsType> newsTypes = new ArrayList<>();
        NewsType info;
        String[] type = setVNNType();
        String[] link = setVNNLink();
        for(int i = 0 ; i < 16 ; i++) {
            info = new NewsType();
            info.setmType(type[i]);
            info.setmLink(link[i]);
            newsTypes.add(info);
        }
        return newsTypes;
    }

    private static String[] setVNNType() {
        String[] type = new String[16];
        type[0] = "Pháp Luật";
        type[1] = "Công Nghệ";
        type[2] = "Kinh Doanh";
        type[3] = "Giáo Dục";
        type[4] = "Thời Sự";
        type[5] = "Giải Trí";
        type[6] = "Sức Khỏe";
        type[7] = "Thể Thao";
        type[8] = "Đời Sống";
        type[9] = "Thế Giới";
        type[10] = "Bất Động Sản";
        type[11] = "Bạn Đọc";
        type[12] = "Tin Mới Nóng";
        type[13] = "Tin Nổi Bật";
        type[14] = "Tuần Việt Nam";
        type[15] = "Goc Nhìn Thẳng";
        return type;
    }

    private static String[] setVNNLink() {
        String[] type = new String[16];
        type[0] = "http://vietnamnet.vn/rss/phap-luat.rss";
        type[1] = "http://vietnamnet.vn/rss/cong-nghe.rss";
        type[2] = "http://vietnamnet.vn/rss/kinh-doanh.rss";
        type[3] = "http://vietnamnet.vn/rss/giao-duc.rss";
        type[4] = "http://vietnamnet.vn/rss/thoi-su.rss";
        type[5] = "http://vietnamnet.vn/rss/giai-tri.rss";
        type[6] = "http://vietnamnet.vn/rss/suc-khoe.rss";
        type[7] = "http://vietnamnet.vn/rss/the-thao.rss";
        type[8] = "http://vietnamnet.vn/rss/doi-song.rss";
        type[9] = "http://vietnamnet.vn/rss/the-gioi.rss";
        type[10] = "http://vietnamnet.vn/rss/bat-dong-san.rss";
        type[11] = "http://vietnamnet.vn/rss/ban-doc.rss";
        type[12] = "http://vietnamnet.vn/rss/moi-nong.rss";
        type[13] = "http://vietnamnet.vn/rss/tin-noi-bat.rss";
        type[14] = "http://vietnamnet.vn/rss/tuanvietnam.rss";
        type[15] = "http://vietnamnet.vn/rss/goc-nhin-thang.rss";
        return type;
    }

    // vnexpress
    public static ArrayList setVNExpressType() {
        ArrayList<NewsType> newsTypes = new ArrayList<>();
        NewsType info;
        String[] type = setVNEType();
        String[] link = setVNELink();
        for(int i = 0 ; i < 16 ; i++) {
            info = new NewsType();
            info.setmType(type[i]);
            info.setmLink(link[i]);
            newsTypes.add(info);
        }
        return newsTypes;
    }

    private static String[] setVNEType() {
        String[] type = new String[16];
        type[0] = "Thời Sự";
        type[1] = "Thế Giới";
        type[2] = "Kinh Doanh";
        type[3] = "Giải Trí";
        type[4] = "Thể Thao";
        type[5] = "Pháp Luật";
        type[6] = "Giáo Dục";
        type[7] = "Sức Khỏe";
        type[8] = "Gia Đình";
        type[9] = "Du Lịch";
        type[10] = "Khoa Học";
        type[11] = "Số Hóa";
        type[12] = "Xe";
        type[13] = "Cộng Đồng";
        type[14] = "Tâm Sự";
        type[15] = "Cười";
        return type;
    }

    private static String[] setVNELink() {
        String[] type = new String[16];
        type[0] = "http://vnexpress.net/rss/thoi-su.rss";
        type[1] = "http://vnexpress.net/rss/the-gioi.rss";
        type[2] = "http://vnexpress.net/rss/kinh-doanh.rss";
        type[3] = "http://vnexpress.net/rss/giai-tri.rss";
        type[4] = "http://vnexpress.net/rss/the-thao.rss";
        type[5] = "http://vnexpress.net/rss/phap-luat.rss";
        type[6] = "http://vnexpress.net/rss/giao-duc.rss";
        type[7] = "http://vnexpress.net/rss/suc-khoe.rss";
        type[8] = "http://vnexpress.net/rss/gia-dinh.rss";
        type[9] = "http://vnexpress.net/rss/du-lich.rss";
        type[10] = "http://vnexpress.net/rss/khoa-hoc.rss";
        type[11] = "http://vnexpress.net/rss/so-hoa.rss";
        type[12] = "http://vnexpress.net/rss/oto-xe-may.rss";
        type[13] = "http://vnexpress.net/rss/cong-dong.rss";
        type[14] = "http://vnexpress.net/rss/tam-su.rss";
        type[15] = "http://vnexpress.net/rss/cuoi.rss";
        return type;
    }
}
